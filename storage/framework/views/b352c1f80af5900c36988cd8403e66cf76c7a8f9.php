<?php $__env->startSection('content'); ?>

    <div class="row">

        <?php if($settingsMode): ?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-vert-padding" >
                <?php echo $__env->make('settings::partial.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        <?php endif; ?>

        <?php if($settingsMode): ?>
            <div  class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-vert-padding">
         <?php endif; ?>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>

                            <div class="header-buttons">
                                <?php if($settingsBackRoute != ''): ?>
                                    <a href="<?php echo e(route($settingsBackRoute)); ?>" title="<?php echo app('translator')->getFromJson('core::core.crud.back'); ?>" class="btn btn-default btn-crud"><?php echo app('translator')->getFromJson('core::core.crud.back'); ?></a>
                                <?php endif; ?>
                            </div>

                            <?php echo app('translator')->getFromJson($language_file.'.title'); ?>
                            <small><?php echo app('translator')->getFromJson($language_file.'.description'); ?></small>
                        </h2>
                    </div>
                    <div class="body">

                        <div class="row">

                            <div class="col-lg-12">

                            <?php echo form_start($form); ?>


                            <?php $__currentLoopData = $show_fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $panelName => $panel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="collapsible">
                                    <?php echo e(Html::section($language_file,$panelName)); ?>


                                    <div class="panel-content">
                                        <?php $__currentLoopData = $panel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fieldName => $options): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <?php if(!isset($options['hide_in_form'])): ?>
                                                <div class="<?php echo e(isset($options['col-class']) ? $options['col-class'] : 'col-lg-6 col-md-6 col-sm-6 col-xs-6'); ?>">

                                                    <?php echo form_row($form->{$fieldName}); ?>

                                                </div>
                                            <?php endif; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </div>
                                </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                            <?php echo form_end($form, $renderRest = true); ?>


                        </div>

                        </div>

                    </div>

                    </div>
                </div>
            </div>
            <?php if($settingsMode): ?>
                </div>
            <?php endif; ?>
    </div>

    <?php $__currentLoopData = $includeViews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php echo $__env->make($v, \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('css'); ?>
    <?php $__currentLoopData = $cssFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <link rel="stylesheet" href="<?php echo Module::asset($moduleName.':css/'.$file); ?>"></link>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>

    <?php $__currentLoopData = $jsFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jsFile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <script src="<?php echo Module::asset($moduleName.':js/'.$jsFile); ?>"></script>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php if($form_request): ?>
<?php echo JsValidator::formRequest($form_request, '#settings_form'); ?>

    <?php endif; ?>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>