<?php $__env->startSection('content'); ?>

    <div class="block-header">
        <h2><?php echo app('translator')->getFromJson('settings::settings.module'); ?></h2>
    </div>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-vert-padding">

            <?php echo $__env->make('settings::partial.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-vert-padding">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <?php echo app('translator')->getFromJson('companies::statistics.title'); ?>
                            <small><?php echo app('translator')->getFromJson('companies::statistics.description'); ?></small>
                        </h2>
                    </div>
                    <div class="body">

                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2 class="card-inside-title"><?php echo app('translator')->getFromJson('companies::statistics.panel.sales'); ?></h2>

                                <div class="panel-content m-t-10">
                                    <div class="col-sm-12 row">

                                        <div id="sale_chart" style="text-align: center; height: 230px">
                                            <?php echo $saleChart->container(); ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2 class="card-inside-title"><?php echo app('translator')->getFromJson('companies::statistics.panel.companies'); ?></h2>

                                <div class="panel-content m-t-10">
                                    <div class="col-sm-12 row">
                                        <div id="registered_chart" style="text-align: center; height: 230px">
                                            <?php echo $registeredChart->container(); ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                <div class="panel-content m-t-10 row">

                                    <div class="col-sm-6">
                                        <h2 class="card-inside-title"><?php echo app('translator')->getFromJson('companies::statistics.panel.popular_packages'); ?></h2>

                                        <div class="panel-content m-t-10">
                                            <div class="col-sm-12 row">
                                                <div id="popuplar_packages" style="text-align: center; height: 230px">
                                                    <?php echo $packagesChart->container(); ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h2 class="card-inside-title"><?php echo app('translator')->getFromJson('companies::statistics.panel.payment_type'); ?></h2>

                                        <div class="panel-content m-t-10">
                                            <div class="col-sm-12 row">
                                                <div id="popuplar_packages" style="text-align: center; height: 230px">
                                                    <?php echo $paymentChart->container(); ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>


    <script type="text/javascript" src="<?php echo e(asset('bap/plugins/chartjs/Chart.bundle.js')); ?>"></script>

    <?php echo $saleChart->script(); ?>

    <?php echo $registeredChart->script(); ?>

    <?php echo $packagesChart->script(); ?>

    <?php echo $paymentChart->script(); ?>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>