<?php $__env->startSection('content'); ?>

    <div class="block-header">
        <h2><?php echo app('translator')->getFromJson('settings::settings.module'); ?></h2>
    </div>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-vert-padding" >

            <?php echo $__env->make('settings::partial.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div  class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-vert-padding">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                        <div class="header">
                            <h2>
                                <?php echo app('translator')->getFromJson('settings::outgoing_server.module'); ?>
                                <small><?php echo app('translator')->getFromJson('settings::outgoing_server.module_description'); ?></small>
                            </h2>
                        </div>
                        <div class="body">
                            <?php echo form($outgoing_server_form); ?>


                            <h2 class="card-inside-title"><?php echo app('translator')->getFromJson('settings::outgoing_server.send_test_email'); ?></h2>

                            <?php echo form($test_email_form); ?>

                        </div>
                    </div>
                </div>
            </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php echo JsValidator::formRequest(\Modules\Platform\Settings\Http\Requests\SaveOutgoingServerRequest::class, '#outgoing_server_form'); ?>

    <?php echo JsValidator::formRequest(\Modules\Platform\Settings\Http\Requests\OutgoingServerTestMailRequest::class, '#outgoing_server_test_email_form'); ?>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>