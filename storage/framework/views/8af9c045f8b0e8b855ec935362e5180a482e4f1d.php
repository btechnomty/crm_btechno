<?php $__env->startSection('content'); ?>

    <div class="block-header">
        <h2><?php echo app('translator')->getFromJson('settings::settings.module'); ?></h2>
    </div>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-vert-padding" >

            <?php echo $__env->make('settings::partial.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div  class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no-vert-padding">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>

                            <div class="header-buttons">
                             <a href="<?php echo e(route($routes['create'])); ?>" class="btn btn-primary btn-create btn-crud"><?php echo app('translator')->getFromJson('core::core.crud.create'); ?></a>
                            </div>

                            <div class="header-text">
                                <?php echo app('translator')->getFromJson($language_file.'.module'); ?>
                                <small><?php echo app('translator')->getFromJson($language_file.'.module_description'); ?></small>
                            </div>

                        </h2>


                    </div>
                    <div class="body">

                        <div class="table-responsive  col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php echo $dataTable->table(['width' => '100%']); ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php $__currentLoopData = $jsFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jsFile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <script src="<?php echo Module::asset($moduleName.':js/'.$jsFile); ?>"></script>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <?php echo $dataTable->scripts(); ?>

<?php $__env->stopPush(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>