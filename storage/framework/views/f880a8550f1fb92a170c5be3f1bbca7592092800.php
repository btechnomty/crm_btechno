<?php $__env->startSection('content'); ?>

    <div class="row">


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <div class="header-buttons">
                            <a href="<?php echo e(route($routes['index'])); ?>" title="<?php echo app('translator')->getFromJson('core::core.crud.back'); ?>" class="btn btn-primary btn-back btn-crud"><?php echo app('translator')->getFromJson('core::core.crud.back'); ?></a>
                        </div>

                        <div class="header-text">
                            <?php echo app('translator')->getFromJson($language_file.'.module'); ?> - <?php echo app('translator')->getFromJson('core::core.crud.import'); ?>
                            <small><?php echo app('translator')->getFromJson($language_file.'.module_description'); ?></small>
                        </div>

                    </h2>


                </div>
                <div class="body">

                    <form class="form-horizontal" method="POST" action="<?php echo e(route($routes['import_process'])); ?>">
                        <?php echo e(csrf_field()); ?>


                        <input type="hidden" name="file_id" value="<?php echo e($csvDataFile->id); ?>" />

                        <div class="table-responsive">


                            <table class="table table-bordered table-striped">

                                <?php if(isset($csv_header)): ?>
                                    <tr>
                                        <?php $__currentLoopData = $csv_header; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $csv_header_field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <th><?php echo e($csv_header_field); ?></th>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tr>
                                <?php endif; ?>

                                <?php $__currentLoopData = $csv_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <td><?php echo e($value); ?></td>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <?php $__currentLoopData = $csv_data[0]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $valueKey =>  $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td>

                                            <select style="min-width: 250px" class="select2" name="fields[<?php echo e($value); ?>]">
                                                    <option value=""><?php echo app('translator')->getFromJson('core::core.empty'); ?></option>
                                                <?php $__currentLoopData = $module_fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mkey => $db_field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <option <?php if(similar_text($valueKey,trans($language_file.'.form.'.$db_field),$perc) && $perc > 60 ): ?> selected="selected" <?php endif; ?> value="<?php echo e($db_field); ?>">

                                                        <?php echo app('translator')->getFromJson($language_file.'.form.'.$db_field); ?> (<?php echo e($db_field); ?>)

                                                    </option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>
                            </table>

                        </div>

                        <br />
                        <input type="submit" class="btn btn-primary" value="<?php echo app('translator')->getFromJson('core::core.btn.process_import'); ?>" />


                    </form>

                </div>
            </div>
        </div>


        <?php $__currentLoopData = $includeViews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo $__env->make($v, \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


        <?php $__env->stopSection(); ?>

        <?php $__env->startPush('css'); ?>
            <?php $__currentLoopData = $cssFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <link rel="stylesheet" href="<?php echo Module::asset($moduleName.':css/'.$file); ?>"></link>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php $__env->stopPush(); ?>

    <?php $__env->startPush('scripts'); ?>
        <?php $__currentLoopData = $jsFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jsFile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <script src="<?php echo Module::asset($moduleName.':js/'.$jsFile); ?>"></script>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php $__env->stopPush(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>